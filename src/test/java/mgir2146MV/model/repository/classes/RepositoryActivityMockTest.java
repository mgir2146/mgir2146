package mgir2146MV.model.repository.classes;

import mgir2146MV.Service.Service;
import mgir2146MV.model.base.Activity;
import mgir2146MV.model.base.Contact;
import mgir2146MV.model.repository.interfaces.RepositoryActivity;
import mgir2146MV.model.repository.interfaces.RepositoryContact;
import mgir2146MV.model.repository.interfaces.RepositoryUser;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class RepositoryActivityMockTest {
    private RepositoryContact repo;
    private RepositoryActivity repoActivity;
    private RepositoryUser repoUser;
    private Service service;

    @Before
    public void setUp() throws Exception {
        this.repo = new RepositoryContactMock();
        this.repoActivity = new RepositoryActivityMock();
        this.repoUser = new RepositoryUserFile();
        this.service = new Service(repo, repoUser, repoActivity);
    }

    @Test
    public void addActivity1()  {
        boolean retValue;
        Date start = new Date(1997,10,24);
        Date end = new Date(1997,10,25);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("a1", start,end,contacts, "desc1");
        retValue = this.repoActivity.addActivity(a1);
        assert(retValue == true);
    }

    @Test
    public void addActivity2() {
        boolean retValue;
        Date startE = new Date(1997,10,24);
        Date endE = new Date(1997,10,25);
        List<Contact> contactsE = new LinkedList<>();

        Activity existed = new Activity("a1", startE,endE,contactsE, "desc1");

        this.repoActivity.addActivity(existed);

        Date start = new Date(1997,10,26);
        Date end = new Date(1997,10,29);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("a1", start,end,contacts, "desc1");

        retValue = this.repoActivity.addActivity(a1);
        assert(retValue == true);
    }

    @Test
    public void addActivity3() {
        boolean retValue;
        Date startE = new Date(1997,10,20);
        Date endE = new Date(1997,10,25);
        List<Contact> contactsE = new LinkedList<>();

        Activity existed = new Activity("a1", startE,endE,contactsE, "desc1");

        this.repoActivity.addActivity(existed);

        Date start = new Date(1997,10,27);
        Date end = new Date(1997,10,25);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("a1", start,end,contacts, "desc1");

        retValue = this.repoActivity.addActivity(a1);
        assert(retValue == true);
    }

    @Test
    public void addActivity4() {
        boolean retValue;
        Date startE = new Date(1997,10,24);
        Date endE = new Date(1997,10,26);
        List<Contact> contactsE = new LinkedList<>();

        Activity existed = new Activity("a1", startE,endE,contactsE, "desc1");

        this.repoActivity.addActivity(existed);

        Date start = new Date(1997,10,24);
        Date end = new Date(1997,10,30);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("a1", start,end,contacts, "desc1");

        retValue = this.repoActivity.addActivity(a1);
        assert(retValue == false);
    }

    @Test
    public void activitiesOnDateValid(){
        boolean retValue;
        Date startE = new Date(1997,10,24);
        Date endE = new Date(1997,10,25);
        List<Contact> contactsE = new LinkedList<>();

        Activity existed = new Activity("a1", startE,endE,contactsE, "desc1");

        this.repoActivity.addActivity(existed);

        Date start = new Date(1997,10,26);
        Date end = new Date(1997,10,29);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("a1", start,end,contacts, "desc1");

        retValue = this.repoActivity.addActivity(a1);
        assert(retValue == true);

        List<Activity> activitiesOnDate = this.repoActivity.activitiesOnDate("a1", startE);
        assert(activitiesOnDate.size() == 1);
    }

    @Test
    public void activitiesOnDateInvalid(){
        Date startE = new Date(1997,10,24);
        List<Activity> activitiesOnDate = this.repoActivity.activitiesOnDate("", startE);
        assert(activitiesOnDate == null);
    }
}