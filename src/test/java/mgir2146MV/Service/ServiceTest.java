package mgir2146MV.Service;

import mgir2146MV.exceptions.InvalidFormatException;
import mgir2146MV.model.base.Activity;
import mgir2146MV.model.base.Contact;
import mgir2146MV.model.repository.classes.RepositoryActivityMock;
import mgir2146MV.model.repository.classes.RepositoryContactMock;
import mgir2146MV.model.repository.classes.RepositoryUserFile;
import mgir2146MV.model.repository.interfaces.RepositoryActivity;
import mgir2146MV.model.repository.interfaces.RepositoryContact;
import mgir2146MV.model.repository.interfaces.RepositoryUser;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class ServiceTest {

    private RepositoryContact repo;
    private RepositoryActivity repoActivity;
    private RepositoryUser repoUser;
    private Service service;

    @Before
    public void setUp() throws Exception {
        this.repo = new RepositoryContactMock();
        this.repoActivity = new RepositoryActivityMock();
        this.repoUser = new RepositoryUserFile();
        this.service = new Service(repo, repoUser, repoActivity);
    }

    @Test
    public void addContact1() throws InvalidFormatException {
        boolean retValue = false;
        retValue = service.addContact("George", "Str.Cimpului", "+40752687974");
        assertEquals(true, retValue);
    }

    @Test
    public void addContact2() throws InvalidFormatException {
        boolean retValue = false;
        retValue = service.addContact("Andrei", "Rasaritului", "12345");
        assertEquals(false, retValue);
    }

    @Test
    public void addContact3() throws InvalidFormatException{
        boolean retValue = false;
        retValue = service.addContact("George", "Str Cimpului","+4012345");
        assertEquals(true, retValue);
    }
    @Test
    public void addContact4() throws InvalidFormatException{
        boolean retValue = false;
        retValue = service.addContact("George", "Str Cimpului","+401234");
        assertEquals(true, retValue);
    }
    @Test
    public void addContact5() throws InvalidFormatException {
        boolean retValue = false;
        retValue = service.addContact("George", "Str Cimpului","");
        assertEquals(false, retValue);
    }
    @Test
    public void addContact6() throws InvalidFormatException{
        boolean retValue = false;
        retValue = service.addContact("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", "Str Cimpului",	"+40728564789");
        assertEquals(true, retValue);
    }
    @Test
    public void addContact7() throws InvalidFormatException {
        boolean retValue;
        retValue = service.addContact("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGgGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", "Str Cimpului",	"+40728564789");
        assertEquals(false, retValue);
    }
    @Test
    public void addContact8() throws InvalidFormatException{
        boolean retValue;
        retValue = service.addContact("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", "Str Cimpului",	"+40728564789");
        assertEquals(true, retValue);
    }

    @Test
    public void addContact9() throws InvalidFormatException{
        boolean retValue;
        retValue = service.addContact("G", "Str Cimpului",	"+40728564789");
        assertEquals(false, retValue);
    }

    @Test
    public void addContact10() throws InvalidFormatException {
        boolean retValue = false;
        retValue = service.addContact("", "Str Cimpului",	"+40728564789");
        assertEquals(false, retValue);
    }
    @Test
    public void addContact11() throws InvalidFormatException {
        boolean retValue = false;
        retValue = service.addContact("George", "Str Cimpului","+40123");
        assertEquals(false, retValue);
    }

    @Test
    public void addContact12() throws InvalidFormatException {

        boolean retValue = false;
        retValue = service.addContact("George", "Str Cimpului","+23456789012345");
        assertEquals(true, retValue);
    }

    @Test
    public void addContact13() throws InvalidFormatException {
        boolean retValue = false;
        retValue = service.addContact("George", "Str Cimpului","+2345678901234");
        assertEquals(true, retValue);
    }

    @Test
    public void addContact14() throws InvalidFormatException {
        boolean retValue = false;
        retValue = service.addContact("George", "Str Cimpului","+234567890123456");
        assertEquals(false, retValue);
    }

    @Test
    public void addContact15() throws InvalidFormatException {
        boolean retValue = false;
        retValue = service.addContact("George", "Str Cimpului","+5401234");
        assertEquals(true, retValue);
    }

    @Test
    public void addContact16() throws InvalidFormatException {
        boolean retValue = false;
        retValue = service.addContact("George", "Str Cimpului","56401234");
        assertEquals(false, retValue);
    }


}