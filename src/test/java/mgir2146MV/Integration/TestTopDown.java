package mgir2146MV.Integration;

import mgir2146MV.Service.Service;
import mgir2146MV.exceptions.InvalidFormatException;
import mgir2146MV.model.base.Activity;
import mgir2146MV.model.base.Contact;
import mgir2146MV.model.repository.classes.RepositoryActivityMock;
import mgir2146MV.model.repository.classes.RepositoryContactMock;
import mgir2146MV.model.repository.classes.RepositoryUserFile;
import mgir2146MV.model.repository.interfaces.RepositoryActivity;
import mgir2146MV.model.repository.interfaces.RepositoryContact;
import mgir2146MV.model.repository.interfaces.RepositoryUser;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class TestTopDown {
    private RepositoryContact repo;
    private RepositoryActivity repoActivity;
    private RepositoryUser repoUser;
    private Service service;

    @Before
    public void setUp() throws Exception {
        this.repo = new RepositoryContactMock();
        this.repoActivity = new RepositoryActivityMock();
        this.repoUser = new RepositoryUserFile();
        this.service = new Service(repo, repoUser, repoActivity);
    }
    // BIG BANG
    @Test
    public void addActivity1()  {
        boolean retValue;
        Date start = new Date(1997,10,24);
        Date end = new Date(1997,10,25);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("a1", start,end,contacts, "desc1");
        retValue = this.repoActivity.addActivity(a1);
        assert(retValue == true);
    }

    @Test
    public void addContact3() throws InvalidFormatException {
        boolean retValue = false;
        Contact c = new Contact("George", "Str Cimpului","+4012345");
        retValue = this.repo.addContact(c);
        assertEquals(true, retValue);
    }

    @Test
    public void activitiesOnDateValid(){
        boolean retValue;
        Date startE = new Date(1997,10,24);
        Date endE = new Date(1997,10,25);
        List<Contact> contactsE = new LinkedList<>();

        Activity existed = new Activity("a1", startE,endE,contactsE, "desc1");

        this.repoActivity.addActivity(existed);

        Date start = new Date(1997,10,26);
        Date end = new Date(1997,10,29);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("a1", start,end,contacts, "desc1");

        retValue = this.repoActivity.addActivity(a1);
        assert(retValue == true);

        List<Activity> activitiesOnDate = this.repoActivity.activitiesOnDate("a1", startE);
        assert(activitiesOnDate.size() == 1);
    }

    @Test
    public void topDownPA() throws InvalidFormatException {
        boolean retValue;
        retValue = service.addActivity("activity1",
                new Date(1997,10,24),
                new Date(1997, 10,26),
                new LinkedList<Contact>(),
                "desc1");
        assertTrue(retValue);

        Date start = new Date(1997,10,27);
        Date end = new Date(1997,10,29);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("activity2", start,end,contacts, "desc1");
        retValue = this.repoActivity.addActivity(a1);
        assertTrue(retValue);
    }

    @Test
    public void topDownPAB() throws InvalidFormatException {
        boolean retValue;
        retValue = service.addActivity("activity1",
                new Date(1997,10,24),
                new Date(1997, 10,26),
                new LinkedList<Contact>(),
                "desc1");
        assertTrue(retValue);

        Date start = new Date(1997,10,27);
        Date end = new Date(1997,10,29);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("activity2", start,end,contacts, "desc1");
        retValue = this.repoActivity.addActivity(a1);
        assertTrue(retValue);

        Contact c = new Contact("George", "Str Cimpului","+4012345");
        retValue = this.repo.addContact(c);
        assertTrue(retValue);
    }

    @Test
    public void topDownPABC() throws InvalidFormatException {
        boolean retValue;
        retValue = service.addActivity("activity1",
                new Date(1997,10,24),
                new Date(1997, 10,26),
                new LinkedList<Contact>(),
                "desc1");
        assertTrue(retValue);

        Date start = new Date(1997,10,27);
        Date end = new Date(1997,10,29);
        List<Contact> contacts = new LinkedList<>();

        Activity a1 = new Activity("activity2", start,end,contacts, "desc1");
        retValue = this.repoActivity.addActivity(a1);
        assertTrue(retValue);

        Contact c = new Contact("George", "Str Cimpului","+4012345");
        retValue = this.repo.addContact(c);
        assertTrue(retValue);

        List<Activity> activitiesOnDate = this.repoActivity.activitiesOnDate("activity1", new Date(1997,10,24));
        assert(activitiesOnDate.size() == 1);
    }
}