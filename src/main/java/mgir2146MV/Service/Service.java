package mgir2146MV.Service;

import mgir2146MV.exceptions.InvalidFormatException;
import mgir2146MV.model.base.Activity;
import mgir2146MV.model.base.Contact;
import mgir2146MV.model.repository.classes.RepositoryActivityFile;
import mgir2146MV.model.repository.classes.RepositoryContactFile;
import mgir2146MV.model.repository.classes.RepositoryUserFile;
import mgir2146MV.model.repository.interfaces.RepositoryActivity;
import mgir2146MV.model.repository.interfaces.RepositoryContact;
import mgir2146MV.model.repository.interfaces.RepositoryUser;

import java.io.BufferedReader;
import java.util.Date;
import java.util.List;

public class Service {
    BufferedReader in = null;

    RepositoryContact contactRep = null;
    RepositoryUser userRep = null;
    RepositoryActivity activityRep = null;

    public Service(RepositoryContact contactRep, RepositoryUser userRep, RepositoryActivity activityRep) {
        this.contactRep = contactRep;
        this.userRep = userRep;
        this.activityRep = activityRep;
    }

    public boolean addContact(String name, String address, String telefon) throws InvalidFormatException {
        if (name.length() < 2 || name.length() > 250) return false;
        if (address.length() < 7) return false;
        if (!telefon.startsWith("+") || (telefon.length() < 7 || telefon.length() > 15) || telefon.contains("[a-zA-Z]"))
            return false;

        Contact contact = new Contact(name, address, telefon);
        this.contactRep.addContact(contact);
        return true;
    }

    public boolean addActivity(String name, Date start, Date end, List<Contact> contacts, String description){
        if (name.length() < 2 || name.length() > 250) return false;
        Activity a = new Activity(name, start, end, contacts, description);
        this.activityRep.addActivity(a);
        return true;
    }

    public List<Activity> getActivitiesOnDate(String name, Date d){
        if (name.length() <= 1) return null;
        return this.activityRep.activitiesOnDate(name, d);
    }

}
