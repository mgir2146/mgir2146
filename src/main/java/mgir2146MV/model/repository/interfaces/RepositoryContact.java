package mgir2146MV.model.repository.interfaces;

import mgir2146MV.model.base.Contact;

import java.util.List;

public interface RepositoryContact {

    List<Contact> getContacts();
    boolean addContact(Contact contact);
    boolean removeContact(Contact contact);
    boolean saveContracts();
    int count();
    Contact getByName(String string);
}
